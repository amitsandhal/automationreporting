package com.mphrx.apiAutomation



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DicrRunController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DicrRun.list(params), model:[dicrRunInstanceCount: DicrRun.count()]
    }

    def show(DicrRun dicrRunInstance) {
        respond dicrRunInstance
    }

    def create() {
        respond new DicrRun(params)
    }

    @Transactional
    def save(DicrRun dicrRunInstance) {
        if (dicrRunInstance == null) {
            notFound()
            return
        }

        if (dicrRunInstance.hasErrors()) {
            respond dicrRunInstance.errors, view:'create'
            return
        }

        dicrRunInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dicrRun.label', default: 'DicrRun'), dicrRunInstance.id])
                redirect dicrRunInstance
            }
            '*' { respond dicrRunInstance, [status: CREATED] }
        }
    }

    def edit(DicrRun dicrRunInstance) {
        respond dicrRunInstance
    }

    @Transactional
    def update(DicrRun dicrRunInstance) {
        if (dicrRunInstance == null) {
            notFound()
            return
        }

        if (dicrRunInstance.hasErrors()) {
            respond dicrRunInstance.errors, view:'edit'
            return
        }

        dicrRunInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DicrRun.label', default: 'DicrRun'), dicrRunInstance.id])
                redirect dicrRunInstance
            }
            '*'{ respond dicrRunInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DicrRun dicrRunInstance) {

        if (dicrRunInstance == null) {
            notFound()
            return
        }

        dicrRunInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DicrRun.label', default: 'DicrRun'), dicrRunInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dicrRun.label', default: 'DicrRun'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
