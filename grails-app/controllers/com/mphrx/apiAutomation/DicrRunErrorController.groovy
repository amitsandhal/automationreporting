package com.mphrx.apiAutomation



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DicrRunErrorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DicrRunError.list(params), model:[dicrRunErrorInstanceCount: DicrRunError.count()]
    }

    def show(DicrRunError dicrRunErrorInstance) {
        respond dicrRunErrorInstance
    }

    def create() {
        respond new DicrRunError(params)
    }

    @Transactional
    def save(DicrRunError dicrRunErrorInstance) {
        if (dicrRunErrorInstance == null) {
            notFound()
            return
        }

        if (dicrRunErrorInstance.hasErrors()) {
            respond dicrRunErrorInstance.errors, view:'create'
            return
        }

        dicrRunErrorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dicrRunError.label', default: 'DicrRunError'), dicrRunErrorInstance.id])
                redirect dicrRunErrorInstance
            }
            '*' { respond dicrRunErrorInstance, [status: CREATED] }
        }
    }

    def edit(DicrRunError dicrRunErrorInstance) {
        respond dicrRunErrorInstance
    }

    @Transactional
    def update(DicrRunError dicrRunErrorInstance) {
        if (dicrRunErrorInstance == null) {
            notFound()
            return
        }

        if (dicrRunErrorInstance.hasErrors()) {
            respond dicrRunErrorInstance.errors, view:'edit'
            return
        }

        dicrRunErrorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DicrRunError.label', default: 'DicrRunError'), dicrRunErrorInstance.id])
                redirect dicrRunErrorInstance
            }
            '*'{ respond dicrRunErrorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DicrRunError dicrRunErrorInstance) {

        if (dicrRunErrorInstance == null) {
            notFound()
            return
        }

        dicrRunErrorInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DicrRunError.label', default: 'DicrRunError'), dicrRunErrorInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dicrRunError.label', default: 'DicrRunError'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
