package com.mphrx.apiAutomation



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ApiRunController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static scaffold = true

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ApiRun.list(params), model:[apiRunInstanceCount: ApiRun.count()]
    }

    def show(ApiRun apiRunInstance) {

        Integer max = params.max?Integer.parseInt(params.max):10
        max = Math.min(max ?: 10, 100)

        Integer offset = params.offset?Integer.parseInt(params.offset):0;

        List<ApiRunResult> apiRunResultList = []
        if (params.query) {
            apiRunInstance?.apiRunResults?.each { ApiRunResult apiRunResult ->
                if (apiRunResult.moduleName.toLowerCase().indexOf(params.query.toLowerCase()) > -1) {
                    apiRunResultList.push(apiRunResult)
                }

            }
            apiRunInstance?.apiRunResults?.each { ApiRunResult apiRunResult ->
                if (apiRunResult.testCaseName.toLowerCase().indexOf(params.query.toLowerCase()) > -1) {
                    apiRunResultList.push(apiRunResult)
                }

            }
            apiRunInstance?.apiRunResults?.each { ApiRunResult apiRunResult ->
                if (apiRunResult.api.toLowerCase().indexOf(params.query.toLowerCase()) > -1) {
                    apiRunResultList.push(apiRunResult)
                }

            }

            apiRunInstance?.apiRunResults?.each { ApiRunResult apiRunResult ->
                if (apiRunResult.result.toLowerCase().indexOf(params.query.toLowerCase()) > -1) {
                    apiRunResultList.push(apiRunResult)
                }

            }

            apiRunInstance?.apiRunResults?.each { ApiRunResult apiRunResult ->
                if (apiRunResult.testcaseId.toLowerCase().equals(params.query.toLowerCase())) {
                    apiRunResultList.push(apiRunResult)
                }

            }

            apiRunInstance?.apiRunResults = []
            apiRunInstance?.apiRunResults = apiRunResultList
        }

        def apiRunResultInstanceCount = apiRunInstance?.apiRunResults?.size()

        apiRunInstance?.apiRunResults  = apiRunInstance?.apiRunResults?.sort{it.id}?.toList()?.subList(offset,Math.min(apiRunInstance?.apiRunResults?.size(),(offset+max)))

        respond apiRunInstance, model:[apiRunResultInstanceCount: apiRunResultInstanceCount] /*, model: [apiRunResultInstanceCount:apiRunResultInstanceCount]*/

    }


    def create() {
        respond new ApiRun(params)
    }

    @Transactional
    def save(ApiRun apiRunInstance) {
        if (apiRunInstance == null) {
            notFound()
            return
        }

        if (apiRunInstance.hasErrors()) {
            respond apiRunInstance.errors, view:'create'
            return
        }

        apiRunInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'apiRun.label', default: 'ApiRun'), apiRunInstance.id])
                redirect apiRunInstance
            }
            '*' { respond apiRunInstance, [status: CREATED] }
        }
    }

    def edit(ApiRun apiRunInstance) {
        respond apiRunInstance
    }

    @Transactional
    def update(ApiRun apiRunInstance) {
        if (apiRunInstance == null) {
            notFound()
            return
        }

        if (apiRunInstance.hasErrors()) {
            respond apiRunInstance.errors, view:'edit'
            return
        }

        apiRunInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ApiRun.label', default: 'ApiRun'), apiRunInstance.id])
                redirect apiRunInstance
            }
            '*'{ respond apiRunInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ApiRun apiRunInstance) {

        if (apiRunInstance == null) {
            notFound()
            return
        }

        apiRunInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ApiRun.label', default: 'ApiRun'), apiRunInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'apiRun.label', default: 'ApiRun'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
