package com.mphrx.apiAutomation



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ApiRunResultController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]



    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ApiRunResult.list(params), model:[apiRunResultInstanceCount: ApiRunResult.count()]
    }

    def show(ApiRunResult apiRunResultInstance) {
        respond apiRunResultInstance
    }

    def create() {
        respond new ApiRunResult(params)
    }

    @Transactional
    def save(ApiRunResult apiRunResultInstance) {
        if (apiRunResultInstance == null) {
            notFound()
            return
        }

        if (apiRunResultInstance.hasErrors()) {
            respond apiRunResultInstance.errors, view:'create'
            return
        }

        apiRunResultInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'apiRunResult.label', default: 'ApiRunResult'), apiRunResultInstance.id])
                redirect apiRunResultInstance
            }
            '*' { respond apiRunResultInstance, [status: CREATED] }
        }
    }

    def edit(ApiRunResult apiRunResultInstance) {
        respond apiRunResultInstance
    }

    @Transactional
    def update(ApiRunResult apiRunResultInstance) {
        if (apiRunResultInstance == null) {
            notFound()
            return
        }

        if (apiRunResultInstance.hasErrors()) {
            respond apiRunResultInstance.errors, view:'edit'
            return
        }

        apiRunResultInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ApiRunResult.label', default: 'ApiRunResult'), apiRunResultInstance.id])
                redirect apiRunResultInstance
            }
            '*'{ respond apiRunResultInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ApiRunResult apiRunResultInstance) {

        if (apiRunResultInstance == null) {
            notFound()
            return
        }

        apiRunResultInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ApiRunResult.label', default: 'ApiRunResult'), apiRunResultInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'apiRunResult.label', default: 'ApiRunResult'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
