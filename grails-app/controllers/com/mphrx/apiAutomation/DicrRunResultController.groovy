package com.mphrx.apiAutomation



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DicrRunResultController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DicrRunResult.list(params), model:[dicrRunResultInstanceCount: DicrRunResult.count()]
    }

    def show(DicrRunResult dicrRunResultInstance) {
        respond dicrRunResultInstance
    }

    def create() {
        respond new DicrRunResult(params)
    }

    @Transactional
    def save(DicrRunResult dicrRunResultInstance) {
        if (dicrRunResultInstance == null) {
            notFound()
            return
        }

        if (dicrRunResultInstance.hasErrors()) {
            respond dicrRunResultInstance.errors, view:'create'
            return
        }

        dicrRunResultInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dicrRunResult.label', default: 'DicrRunResult'), dicrRunResultInstance.id])
                redirect dicrRunResultInstance
            }
            '*' { respond dicrRunResultInstance, [status: CREATED] }
        }
    }

    def edit(DicrRunResult dicrRunResultInstance) {
        respond dicrRunResultInstance
    }

    @Transactional
    def update(DicrRunResult dicrRunResultInstance) {
        if (dicrRunResultInstance == null) {
            notFound()
            return
        }

        if (dicrRunResultInstance.hasErrors()) {
            respond dicrRunResultInstance.errors, view:'edit'
            return
        }

        dicrRunResultInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DicrRunResult.label', default: 'DicrRunResult'), dicrRunResultInstance.id])
                redirect dicrRunResultInstance
            }
            '*'{ respond dicrRunResultInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DicrRunResult dicrRunResultInstance) {

        if (dicrRunResultInstance == null) {
            notFound()
            return
        }

        dicrRunResultInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DicrRunResult.label', default: 'DicrRunResult'), dicrRunResultInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dicrRunResult.label', default: 'DicrRunResult'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
