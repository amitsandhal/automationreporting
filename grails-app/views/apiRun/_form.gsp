<%@ page import="com.mphrx.apiAutomation.ApiRun" %>



<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'apiRunResults', 'error')} ">
	<label for="apiRunResults">
		<g:message code="apiRun.apiRunResults.label" default="Api Run Results" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${apiRunInstance?.apiRunResults?}" var="a">
    <li><g:link controller="apiRunResult" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="apiRunResult" action="create" params="['apiRun.id': apiRunInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'apiRunResult.label', default: 'ApiRunResult')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'baseEnvironmentUrl', 'error')} required">
	<label for="baseEnvironmentUrl">
		<g:message code="apiRun.baseEnvironmentUrl.label" default="Base Environment Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="baseEnvironmentUrl" required="" value="${apiRunInstance?.baseEnvironmentUrl}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'baseEnvironmentVersion', 'error')} required">
	<label for="baseEnvironmentVersion">
		<g:message code="apiRun.baseEnvironmentVersion.label" default="Base Environment Version" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="baseEnvironmentVersion" required="" value="${apiRunInstance?.baseEnvironmentVersion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'endTime', 'error')} required">
	<label for="endTime">
		<g:message code="apiRun.endTime.label" default="End Time" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="endTime" precision="day"  value="${apiRunInstance?.endTime}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'executor', 'error')} required">
	<label for="executor">
		<g:message code="apiRun.executor.label" default="Executor" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="executor" required="" value="${apiRunInstance?.executor}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'startTime', 'error')} required">
	<label for="startTime">
		<g:message code="apiRun.startTime.label" default="Start Time" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="startTime" precision="day"  value="${apiRunInstance?.startTime}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'testEnvironmentUrl', 'error')} required">
	<label for="testEnvironmentUrl">
		<g:message code="apiRun.testEnvironmentUrl.label" default="Test Environment Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testEnvironmentUrl" required="" value="${apiRunInstance?.testEnvironmentUrl}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunInstance, field: 'testEnvironmentVersion', 'error')} required">
	<label for="testEnvironmentVersion">
		<g:message code="apiRun.testEnvironmentVersion.label" default="Test Environment Version" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testEnvironmentVersion" required="" value="${apiRunInstance?.testEnvironmentVersion}"/>

</div>

