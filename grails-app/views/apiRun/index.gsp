
<%@ page import="com.mphrx.apiAutomation.ApiRun" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apiRun.label', default: 'API Runs')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-apiRun" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-apiRun" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<g:sortableColumn property="apiRunId" title="${message(code: 'apiRun.apiRunId.label', default: 'Run Id')}" />

						<g:sortableColumn property="baseEnvironmentUrl" title="${message(code: 'apiRun.baseEnvironmentUrl.label', default: 'Base Env Url')}" />
					
						<g:sortableColumn property="baseEnvironmentVersion" title="${message(code: 'apiRun.baseEnvironmentVersion.label', default: 'Base Env Version')}" />

						<g:sortableColumn property="testEnvironmentUrl" title="${message(code: 'apiRun.testEnvironmentUrl.label', default: 'Test Env Url')}" />

						<g:sortableColumn property="testEnvironmentVersion" title="${message(code: 'apiRun.testEnvironmentVersion.label', default: 'Test Env Version')}" />

%{--
						<g:sortableColumn property="executor" title="${message(code: 'apiRun.executor.label', default: 'Executor')}" />
--}%

						<g:sortableColumn property="startTime" title="${message(code: 'apiRun.startTime.label', default: 'Start Time')}" />

						<g:sortableColumn property="endTime" title="${message(code: 'apiRun.endTime.label', default: 'End Time')}" />

						<g:sortableColumn property="endTime" title="${message(code: 'apiRun.goToTestResults.label', default: 'Results')}" />

						%{--
                                                <g:sortableColumn property="dateCreated" title="${message(code: 'apiRun.dateCreated.label', default: 'Date Created')}" />
                        --}%



%{--
						<g:sortableColumn property="lastUpdated" title="${message(code: 'apiRun.lastUpdated.label', default: 'Last Updated')}" />
--}%

					</tr>
				</thead>
				<tbody>
				<g:each in="${apiRunInstanceList}" status="i" var="apiRunInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${fieldValue(bean: apiRunInstance, field: "id")}</td>

						<td>${fieldValue(bean: apiRunInstance, field: "baseEnvironmentUrl")}</td>
					
						<td>${fieldValue(bean: apiRunInstance, field: "baseEnvironmentVersion")}</td>

						<td>${fieldValue(bean: apiRunInstance, field: "testEnvironmentUrl")}</td>

						<td>${fieldValue(bean: apiRunInstance, field: "testEnvironmentVersion")}</td>

%{--
						<td>${fieldValue(bean: apiRunInstance, field: "executor")}</td>
--}%

						<td><g:formatDate date="${apiRunInstance.startTime}" /></td>
					
						<td><g:formatDate date="${apiRunInstance.endTime}" /></td>

						<td><g:link action="show" id="${apiRunInstance.id}">See Results</g:link></td>


					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${apiRunInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
