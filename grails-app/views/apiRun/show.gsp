<%@ page import="com.mphrx.apiAutomation.ApiRun" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'apiRun.label', default: 'API Test Run Results')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<link rel="stylesheet" href="${resource(dir: 'www/lib/codemirror/lib',file: 'codemirror.css')}" charset="utf-8">
	<link rel="stylesheet" href="${resource(dir: 'www/lib/codemirror/theme',file: 'tomorrow-night.css')}" charset="utf-8">
	<link rel="stylesheet" href="${resource(dir: 'css',file: 'main.css')}" charset="utf-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">


	<script src="${resource(dir:'jdd',file:'jquery-1.12.4.js')}"></script>
	<script src="${resource(dir:'jdd',file:'jquery-ui.js')}"></script>

	<script src="${resource(dir:'jdd',file:'underscore-min.js')}" type="text/javascript" charset="utf-8"></script>

	<link rel="stylesheet" href="${resource(dir:'jdd',file:'reset.css')}" type="text/css" media="screen">
	<link rel="stylesheet" href="${resource(dir:'jdd',file:'throbber.css')}" type="text/css" media="screen">
	<link rel="stylesheet" href="${resource(dir:'jdd',file:'jdd.css')}" type="text/css" media="screen">

	<script src="${resource(dir:'jdd/jsl',file:'jsl.format.js')}" type="text/javascript" charset="utf-8"></script>
	<script src="${resource(dir:'jdd/jsl',file:'jsl.parser.js')}" type="text/javascript" charset="utf-8"></script>
	<script src="${resource(dir:'jdd',file:'jdd.js')}" type="text/javascript" charset="utf-8"></script>

	<style>
	.CodeMirror {
		border: 1px solid #eee;
		min-height:500px;
	}
	</style>
</head>
<body>
<a href="#show-apiRun" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
	<ul>
		<li><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link action="index">API Runs</g:link></li>

	</ul>
</div>
<div id="show-apiRun" class="content scaffold-show" role="main">
	<h1><g:message code="default.show.label" args="[entityName]" /></h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<fieldset class="form">
		<g:form name="searchForm" action="show" method="GET" id="${apiRunInstance.id}">
			<div class="fieldcontain">
				<g:textField name="query" value="${params.query}"/>
				<g:submitButton id="submitButton" name="Search" value="Search"></g:submitButton>
				<input type="button" onclick="clearFields()" value="Reset">
				Showing ${apiRunResultInstanceCount} results
			</div>

		</g:form>
	</fieldset>
	%{--			<div>

                </div>--}%


	<table width="100%" style="table-layout: fixed; width: 100%">
		<th width="10%">Test ID</th>
		<th width="22%">Test Case Name</th>
		<th width="15%" style="word-wrap: break-word">Module Name</th>
		<th width="25%" style="word-wrap: break-word">API</th>
		<th width="7%">Payload</th>
		<th width="7%">Result</th>
		<th width="7%">Error</th>
		<th width="7%">Compare Responses</th>


		<g:each in="${apiRunInstance.apiRunResults.sort({it.id})}" var="apiRunResult">
			<tr style="word-wrap: break-word">
				<td>${apiRunResult.testcaseId}</td>
				<td>${apiRunResult.testCaseName}</td>
				<td>${apiRunResult.moduleName}</td>
				<td style="word-wrap: break-word" width="30%">${apiRunResult.api}</td>

				<td><button style="height:40px;width:80px" onclick="showPayload('${apiRunResult.payload}')">Show Payload</button></td>
				<td >${apiRunResult.result}</td>

				<g:if test="${apiRunResult.error=="No Error"}">
					<td><button style="height:40px;width:80px" disabled="disabled"  onclick="showErrorMessage('${URLEncoder.encode(apiRunResult.error,'UTF-8').replace("+", "%20")}')"> No  Error </button></td>
				</g:if>
				<g:else>
					<td><button style="height:40px;width:80px" onclick="showErrorMessage('${URLEncoder.encode(apiRunResult.error,'UTF-8').replace("+", "%20")}')">Show Error</button></td>
				</g:else>

				<g:if test="${(apiRunResult.baseResponse as java.util.Map).size()>3 && (apiRunResult.testResponse as java.util.Map).size()>3}">
					<td><button style="height:40px;width:82px" onclick="showJSONDifference(${apiRunResult.baseResponse},${apiRunResult.testResponse})">Compare Responses</button></td>
				</g:if>
				<g:else>
					<td><button style="height:40px;width:82px" disabled="disabled" onclick="showJSONDifference(${apiRunResult.baseResponse},${apiRunResult.testResponse})">Nothing to Compare
					</button></td>
				</g:else>

			</tr>

		</g:each>
	</table>
	<div class="pagination">
		<g:paginate params="${[query:params.query]}" id="${apiRunInstance.id}" total="${apiRunResultInstanceCount ?: 0}" />
	</div>

	<header id="header">

		<div class="right">
			<span id="error-message" style="display:none;">Invalid JSON</span>
		</div>
	</header>


</div>

<!-- The Modal -->
<div id="myModal">


</div>

<div id="main" style="width: 0px;height: 0px;display: none;">
	<div class="initContainer">
		<div class="left">
			<textarea spellcheck="false" id="textarealeft" placeholder="Enter JSON to compare, enter an URL to JSON"></textarea>
			<pre id="errorLeft" class="error"></pre>
			<span class="fileInput">or <input type="file" id="fileLeft" onchange="jdd.handleFiles(this.files, 'left')"></span>
		</div>

		<div class="center">
			<button id="compare">Compare</button>
			<div class="throbber-loader"></div>
			<br/><br/><br/><br/>
			or try some <a href="#" id="sample">sample data</a>

		</div>

		<div class="right">
			<textarea spellcheck="false" class="right" id="textarearight" placeholder="Enter JSON to compare, enter an URL to JSON"></textarea>
			<pre id="errorRight" class="error"></pre>
			<span class="fileInput">or <input type="file" id="fileRight" onchange="jdd.handleFiles(this.files, 'right')"></span>
		</div>
	</div>

	<div class="diffcontainer">
		<div id="report">
		</div>
		<pre id="out" class="left" class="codeBlock"></pre>
		<pre id="out2" class="right" class="codeBlock"></pre>
		<ul id="toolbar" class="toolbar"></ul>
	</div>
</div>


<script src="${resource(dir: 'www/lib/fast-json-patch/dist/',file: 'json-patch-duplex.min.js')}" charset="utf-8"></script>
<script src="${resource(dir: 'www/lib/backbone-events-standalone/',file: 'backbone-events-standalone.min.js')}" charset="utf-8"></script>

<!-- Requires CodeMirror -->
<script type="text/javascript" src="${resource(dir: 'Mergely/lib',file: 'codemirror.js')}"></script>
<link type="text/css" rel="stylesheet" href="${resource(dir: 'Mergely/lib',file: 'codemirror.css')}" />

<!-- Requires Mergely -->
<script type="text/javascript" src="${resource(dir: 'Mergely/lib',file: 'mergely.js')}"></script>
<link type="text/css" rel="stylesheet" href="${resource(dir: 'Mergely/lib',file: 'mergely.css')}" />
<script src="${resource(dir: 'js/',file: 'main.js')}" charset="utf-8"></script>


<script type="text/javascript">
	function closeDialog(){
		$('#main').dialog('close');
	}

	function showJSONDifference(json1,json2){

		json1 = JSON.parse(JSON.stringify(json1).replace(/\\n/g,'!NEWLINE!'))
		json2 = JSON.parse(JSON.stringify(json2).replace(/\\n/g,'!NEWLINE!'))

			$('#textarealeft').val(JSON.stringify(json1,null,4));

			$('#textarealeft').val(JSON.stringify(json1,null,4).replace(/!NEWLINE!/g,'\\\\n'));

			$('#textarearight').val(JSON.stringify(json2,null,4).replace(/!NEWLINE!/g,'\\\\n'));

		try{
			jdd.compare();
		}catch (e){
			console.error(e)
			if($("#" + "message").length == 0) {
				$('#main').prepend("<div id='message' style='text-align: center;'><span style='color:red'>Please compare manually as tool is facing some issues with comparison. Please see console logs for details. </span></div>")
			}
		}

		$('#main').dialog({
			width: "82%",
			height: '650',
			resizable:false,
			left:'9%'
		});
	}

	function showErrorMessage(error) {
		$( "#myModal" ).html("<pre></pre>")
		$( "#myModal pre" ).text(decodeURIComponent(error));
		$( "#myModal" ).dialog({width: "90%",
			maxWidth: "768px",
			height:'580'});
	}

	function showPayload(payload) {
		payload=JSON.parse(payload)
		$( "#myModal" ).html("<pre></pre>")
		$( "#myModal pre" ).text(decodeURIComponent(JSON.stringify(payload,null,4)));
		$( "#myModal" ).dialog({width: "90%",
			maxWidth: "768px",
			height:'580'});
	}


	function clearFields() {
		document.getElementsByName("query")[0].value=""
		document.getElementById("submitButton").click()
	}

</script>
</body>
</html>
