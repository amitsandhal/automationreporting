
<%@ page import="com.mphrx.apiAutomation.ApiRunResult" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apiRunResult.label', default: 'ApiRunResult')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-apiRunResult" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-apiRunResult" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list apiRunResult">
			
				<g:if test="${apiRunResultInstance?.api}">
				<li class="fieldcontain">
					<span id="api-label" class="property-label"><g:message code="apiRunResult.api.label" default="Api" /></span>
					
						<span class="property-value" aria-labelledby="api-label"><g:fieldValue bean="${apiRunResultInstance}" field="api"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.apiRun}">
				<li class="fieldcontain">
					<span id="apiRun-label" class="property-label"><g:message code="apiRunResult.apiRun.label" default="Api Run" /></span>
					
						<span class="property-value" aria-labelledby="apiRun-label"><g:link controller="apiRun" action="show" id="${apiRunResultInstance?.apiRun?.id}">${apiRunResultInstance?.apiRun?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.baseResponse}">
				<li class="fieldcontain">
					<span id="baseResponse-label" class="property-label"><g:message code="apiRunResult.baseResponse.label" default="Base Response" /></span>
					
						<span class="property-value" aria-labelledby="baseResponse-label"><g:fieldValue bean="${apiRunResultInstance}" field="baseResponse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.baseResponseCode}">
				<li class="fieldcontain">
					<span id="baseResponseCode-label" class="property-label"><g:message code="apiRunResult.baseResponseCode.label" default="Base Response Code" /></span>
					
						<span class="property-value" aria-labelledby="baseResponseCode-label"><g:fieldValue bean="${apiRunResultInstance}" field="baseResponseCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="apiRunResult.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${apiRunResultInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.error}">
				<li class="fieldcontain">
					<span id="error-label" class="property-label"><g:message code="apiRunResult.error.label" default="Error" /></span>
					
						<span class="property-value" aria-labelledby="error-label"><g:fieldValue bean="${apiRunResultInstance}" field="error"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="apiRunResult.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${apiRunResultInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.moduleName}">
				<li class="fieldcontain">
					<span id="moduleName-label" class="property-label"><g:message code="apiRunResult.moduleName.label" default="Module Name" /></span>
					
						<span class="property-value" aria-labelledby="moduleName-label"><g:fieldValue bean="${apiRunResultInstance}" field="moduleName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.payload}">
				<li class="fieldcontain">
					<span id="payload-label" class="property-label"><g:message code="apiRunResult.payload.label" default="Payload" /></span>
					
						<span class="property-value" aria-labelledby="payload-label"><g:fieldValue bean="${apiRunResultInstance}" field="payload"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.result}">
				<li class="fieldcontain">
					<span id="result-label" class="property-label"><g:message code="apiRunResult.result.label" default="Result" /></span>
					
						<span class="property-value" aria-labelledby="result-label"><g:fieldValue bean="${apiRunResultInstance}" field="result"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.testCaseName}">
				<li class="fieldcontain">
					<span id="testCaseName-label" class="property-label"><g:message code="apiRunResult.testCaseName.label" default="Test Case Name" /></span>
					
						<span class="property-value" aria-labelledby="testCaseName-label"><g:fieldValue bean="${apiRunResultInstance}" field="testCaseName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.testResonseCode}">
				<li class="fieldcontain">
					<span id="testResonseCode-label" class="property-label"><g:message code="apiRunResult.testResonseCode.label" default="Test Resonse Code" /></span>
					
						<span class="property-value" aria-labelledby="testResonseCode-label"><g:fieldValue bean="${apiRunResultInstance}" field="testResonseCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apiRunResultInstance?.testResponse}">
				<li class="fieldcontain">
					<span id="testResponse-label" class="property-label"><g:message code="apiRunResult.testResponse.label" default="Test Response" /></span>
					
						<span class="property-value" aria-labelledby="testResponse-label"><g:fieldValue bean="${apiRunResultInstance}" field="testResponse"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:apiRunResultInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${apiRunResultInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
