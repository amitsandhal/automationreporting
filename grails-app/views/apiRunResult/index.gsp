
<%@ page import="com.mphrx.apiAutomation.ApiRunResult" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apiRunResult.label', default: 'ApiRunResult')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<link rel="stylesheet" href="../online-json-diff-master/www/lib/codemirror/lib/codemirror.css" charset="utf-8">
		<link rel="stylesheet" href="../online-json-diff-master/www/lib/codemirror/theme/tomorrow-night.css" charset="utf-8">
		<link rel="stylesheet" href="../online-json-diff-master/css/main.css" charset="utf-8">

	</head>
	<body>
		<a href="#list-apiRunResult" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-apiRunResult" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="api" title="${message(code: 'apiRunResult.api.label', default: 'Api')}" />
					
						<th><g:message code="apiRunResult.apiRun.label" default="Api Run" /></th>
					
						<g:sortableColumn property="baseResponse" title="${message(code: 'apiRunResult.baseResponse.label', default: 'Base Response')}" />
					
						<g:sortableColumn property="baseResponseCode" title="${message(code: 'apiRunResult.baseResponseCode.label', default: 'Base Response Code')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'apiRunResult.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="error" title="${message(code: 'apiRunResult.error.label', default: 'Error')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${apiRunResultInstanceList}" status="i" var="apiRunResultInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${apiRunResultInstance.id}">${fieldValue(bean: apiRunResultInstance, field: "api")}</g:link></td>
					
						<td>${fieldValue(bean: apiRunResultInstance, field: "apiRun")}</td>
					
						<td>${fieldValue(bean: apiRunResultInstance, field: "baseResponse")}</td>
					
						<td>${fieldValue(bean: apiRunResultInstance, field: "baseResponseCode")}</td>
					
						<td><g:formatDate date="${apiRunResultInstance.dateCreated}" /></td>
					
						<td>${fieldValue(bean: apiRunResultInstance, field: "error")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${apiRunResultInstanceCount ?: 0}" />
			</div>
		</div>
	<script src="../online-json-diff-master/www/lib/fast-json-patch/dist/json-patch-duplex.min.js" charset="utf-8"></script>
	<script src="../online-json-diff-master/www/lib/backbone-events-standalone/backbone-events-standalone.min.js" charset="utf-8"></script>
	<script src="../online-json-diff-master/www/lib/codemirror/lib/codemirror.js" charset="utf-8"></script>
	<script src="../online-json-diff-master/www/lib/codemirror/lib/util/formatting.js" charset="utf-8"></script>
	<script src="../online-json-diff-master/www/lib/codemirror/mode/javascript/javascript.js" charset="utf-8"></script>
	<script src="../online-json-diff-master/www/lib/codemirror/addon/edit/matchbrackets.js" charset="utf-8"></script>
	<script src="../online-json-diff-master/js/main.js" charset="utf-8"></script>
	</body>
</html>
