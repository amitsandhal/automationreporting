<%@ page import="com.mphrx.apiAutomation.ApiRunResult" %>



<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'api', 'error')} required">
	<label for="api">
		<g:message code="apiRunResult.api.label" default="Api" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="api" required="" value="${apiRunResultInstance?.api}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'apiRun', 'error')} required">
	<label for="apiRun">
		<g:message code="apiRunResult.apiRun.label" default="Api Run" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="apiRun" name="apiRun.id" from="${com.mphrx.apiAutomation.ApiRun.list()}" optionKey="id" required="" value="${apiRunResultInstance?.apiRun?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'baseResponse', 'error')} required">
	<label for="baseResponse">
		<g:message code="apiRunResult.baseResponse.label" default="Base Response" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="baseResponse" required="" value="${apiRunResultInstance?.baseResponse}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'baseResponseCode', 'error')} required">
	<label for="baseResponseCode">
		<g:message code="apiRunResult.baseResponseCode.label" default="Base Response Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="baseResponseCode" required="" value="${apiRunResultInstance?.baseResponseCode}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'error', 'error')} required">
	<label for="error">
		<g:message code="apiRunResult.error.label" default="Error" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="error" required="" value="${apiRunResultInstance?.error}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'moduleName', 'error')} required">
	<label for="moduleName">
		<g:message code="apiRunResult.moduleName.label" default="Module Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="moduleName" required="" value="${apiRunResultInstance?.moduleName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'payload', 'error')} required">
	<label for="payload">
		<g:message code="apiRunResult.payload.label" default="Payload" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="payload" required="" value="${apiRunResultInstance?.payload}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'result', 'error')} required">
	<label for="result">
		<g:message code="apiRunResult.result.label" default="Result" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="result" required="" value="${apiRunResultInstance?.result}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'testCaseName', 'error')} required">
	<label for="testCaseName">
		<g:message code="apiRunResult.testCaseName.label" default="Test Case Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testCaseName" required="" value="${apiRunResultInstance?.testCaseName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'testResonseCode', 'error')} required">
	<label for="testResonseCode">
		<g:message code="apiRunResult.testResonseCode.label" default="Test Resonse Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testResonseCode" required="" value="${apiRunResultInstance?.testResonseCode}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: apiRunResultInstance, field: 'testResponse', 'error')} required">
	<label for="testResponse">
		<g:message code="apiRunResult.testResponse.label" default="Test Response" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testResponse" required="" value="${apiRunResultInstance?.testResponse}"/>

</div>

