
<%@ page import="com.mphrx.apiAutomation.DicrRunError" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dicrRunError.label', default: 'DicrRunError')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-dicrRunError" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-dicrRunError" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list dicrRunError">
			
				<g:if test="${dicrRunErrorInstance?.collectionName}">
				<li class="fieldcontain">
					<span id="collectionName-label" class="property-label"><g:message code="dicrRunError.collectionName.label" default="Collection Name" /></span>
					
						<span class="property-value" aria-labelledby="collectionName-label"><g:fieldValue bean="${dicrRunErrorInstance}" field="collectionName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dicrRunErrorInstance?.comparisonError}">
				<li class="fieldcontain">
					<span id="comparisonError-label" class="property-label"><g:message code="dicrRunError.comparisonError.label" default="Comparison Error" /></span>
					
						<span class="property-value" aria-labelledby="comparisonError-label"><g:fieldValue bean="${dicrRunErrorInstance}" field="comparisonError"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dicrRunErrorInstance?.countMismatchError}">
				<li class="fieldcontain">
					<span id="countMismatchError-label" class="property-label"><g:message code="dicrRunError.countMismatchError.label" default="Count Mismatch Error" /></span>
					
						<span class="property-value" aria-labelledby="countMismatchError-label"><g:fieldValue bean="${dicrRunErrorInstance}" field="countMismatchError"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dicrRunErrorInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="dicrRunError.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${dicrRunErrorInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dicrRunErrorInstance?.dicrRunResults}">
				<li class="fieldcontain">
					<span id="dicrRunResults-label" class="property-label"><g:message code="dicrRunError.dicrRunResults.label" default="Dicr Run Results" /></span>
					
						<span class="property-value" aria-labelledby="dicrRunResults-label"><g:link controller="dicrRunResult" action="show" id="${dicrRunErrorInstance?.dicrRunResults?.id}">${dicrRunErrorInstance?.dicrRunResults?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${dicrRunErrorInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="dicrRunError.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${dicrRunErrorInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:dicrRunErrorInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${dicrRunErrorInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
