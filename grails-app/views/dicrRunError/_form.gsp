<%@ page import="com.mphrx.apiAutomation.DicrRunError" %>



<div class="fieldcontain ${hasErrors(bean: dicrRunErrorInstance, field: 'collectionName', 'error')} required">
	<label for="collectionName">
		<g:message code="dicrRunError.collectionName.label" default="Collection Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="collectionName" required="" value="${dicrRunErrorInstance?.collectionName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunErrorInstance, field: 'comparisonError', 'error')} ">
	<label for="comparisonError">
		<g:message code="dicrRunError.comparisonError.label" default="Comparison Error" />
		
	</label>
	

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunErrorInstance, field: 'countMismatchError', 'error')} required">
	<label for="countMismatchError">
		<g:message code="dicrRunError.countMismatchError.label" default="Count Mismatch Error" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="countMismatchError" required="" value="${dicrRunErrorInstance?.countMismatchError}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunErrorInstance, field: 'dicrRunResults', 'error')} required">
	<label for="dicrRunResults">
		<g:message code="dicrRunError.dicrRunResults.label" default="Dicr Run Results" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="dicrRunResults" name="dicrRunResults.id" from="${com.mphrx.apiAutomation.DicrRunResult.list()}" optionKey="id" required="" value="${dicrRunErrorInstance?.dicrRunResults?.id}" class="many-to-one"/>

</div>

