
<%@ page import="com.mphrx.apiAutomation.DicrRun" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dicrRun.label', default: 'Dicr Runs')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-dicrRun" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-dicrRun" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<g:sortableColumn property="dicrRunId" title="${message(code: 'apiRun.dicrRunId.label', default: 'Run Id')}" />
					
						<g:sortableColumn property="baseServer" title="${message(code: 'dicrRun.baseServer.label', default: 'Base Server')}" />
					
						<g:sortableColumn property="baseVersion" title="${message(code: 'dicrRun.baseVersion.label', default: 'Base Version')}" />

						<g:sortableColumn property="testServer" title="${message(code: 'dicrRun.testServer.label', default: 'Test Server')}" />

						<g:sortableColumn property="testersion" title="${message(code: 'dicrRun.testersion.label', default: 'Test Version')}" />

						<g:sortableColumn property="startTime" title="${message(code: 'dicrRun.startTime.label', default: 'Start Time')}" />

						<g:sortableColumn property="endTime" title="${message(code: 'dicrRun.endTime.label', default: 'End Time')}" />

						<g:sortableColumn property="result" title="${message(code: 'apiRun.goToTestResults.label', default: 'Results')}" />


					</tr>
				</thead>
				<tbody>
				<g:each in="${dicrRunInstanceList}" status="i" var="dicrRunInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${fieldValue(bean: dicrRunInstance, field: "id")}</td>

						<td>${fieldValue(bean: dicrRunInstance, field: "baseServer")}</td>
					
						<td>${fieldValue(bean: dicrRunInstance, field: "baseVersion")}</td>

						<td>${fieldValue(bean: dicrRunInstance, field: "testServer")}</td>

						<td>${fieldValue(bean: dicrRunInstance, field: "testVersion")}</td>

						<td><g:formatDate date="${dicrRunInstance.startTime}" /></td>
					
						<td><g:formatDate date="${dicrRunInstance.endTime}" /></td>

						<td><g:link action="show" id="${dicrRunInstance.id}">See Results</g:link></td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${dicrRunInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
