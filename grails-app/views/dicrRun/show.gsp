
<%@ page import="com.mphrx.apiAutomation.DicrRun" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dicrRun.label', default: 'Dicr Run Results')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="nav" role="navigation">
		<ul>
			<li><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			<li><g:link action="index">Dicr Runs</g:link></li>
		</ul>
	</div>
		<div id="show-dicrRun" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<table width="100%" style="table-layout: fixed; width: 100%;word-wrap: break-word">
			<tr>
				<th width="19%" >Test Case Name</th>
				<th width="10%" >Base Env Collections Count</th>
				<th width="10%" >Test Env Collections Count</th>
				<th width="10%" >Common Collections Count</th>
				<th width="18%" >Collections Only In Base</th>
				<th width="18%" >Collections Only In Test</th>
				<th width="8%">Result</th>
				<th width="7%">Errors</th>
			</tr>
				<g:each in="${dicrRunInstance.dicrRunResults.sort({it.id})}" var="dicrRunResult">
					<tr>
						<td>${dicrRunResult.testCaseName}</td>
						<td>${dicrRunResult.collectionsCountInBaseEnvironment}</td>
						<td>${dicrRunResult.collectionsCountInTestEnvironment}</td>
						<td>${dicrRunResult.commonCollectionsCount}</td>
						<td style="word-wrap: break-word" width="30%">
							<g:if test="${dicrRunResult.collectionsInBaseButNotInTest == null || dicrRunResult.collectionsInBaseButNotInTest.isEmpty()}">
								<p style="margin-left: 5.5em">-</p>
							</g:if>
							<g:else>
								${dicrRunResult.collectionsInBaseButNotInTest}
							</g:else>

						</td>
						<td style="word-wrap: break-word" width="30%">
							<g:if test="${dicrRunResult.collectionsInTestButNotInBase == null || dicrRunResult.collectionsInTestButNotInBase.isEmpty()}">
								<p style="margin-left: 5.5em">-</p>
							</g:if>
							<g:else>
								${dicrRunResult.collectionsInTestButNotInBase}
							</g:else>
						</td>
						<td>${dicrRunResult.result}</td>
						<td><g:link action="show" controller="DicrRunResult" id="${dicrRunResult.id}">See Errors</g:link></td>

					</tr>

				</g:each>
			</table>

		</div>
	</body>
</html>
