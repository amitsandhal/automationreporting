<%@ page import="com.mphrx.apiAutomation.DicrRun" %>



<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'baseServer', 'error')} required">
	<label for="baseServer">
		<g:message code="dicrRun.baseServer.label" default="Base Server" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="baseServer" required="" value="${dicrRunInstance?.baseServer}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'baseVersion', 'error')} required">
	<label for="baseVersion">
		<g:message code="dicrRun.baseVersion.label" default="Base Version" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="baseVersion" required="" value="${dicrRunInstance?.baseVersion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'dicrRunResults', 'error')} ">
	<label for="dicrRunResults">
		<g:message code="dicrRun.dicrRunResults.label" default="Dicr Run Results" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${dicrRunInstance?.dicrRunResults?}" var="d">
    <li><g:link controller="dicrRunResult" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="dicrRunResult" action="create" params="['dicrRun.id': dicrRunInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'dicrRunResult.label', default: 'DicrRunResult')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'endTime', 'error')} required">
	<label for="endTime">
		<g:message code="dicrRun.endTime.label" default="End Time" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="endTime" precision="day"  value="${dicrRunInstance?.endTime}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'executor', 'error')} required">
	<label for="executor">
		<g:message code="dicrRun.executor.label" default="Executor" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="executor" required="" value="${dicrRunInstance?.executor}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'startTime', 'error')} required">
	<label for="startTime">
		<g:message code="dicrRun.startTime.label" default="Start Time" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="startTime" precision="day"  value="${dicrRunInstance?.startTime}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'testServer', 'error')} required">
	<label for="testServer">
		<g:message code="dicrRun.testServer.label" default="Test Server" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testServer" required="" value="${dicrRunInstance?.testServer}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunInstance, field: 'testVersion', 'error')} required">
	<label for="testVersion">
		<g:message code="dicrRun.testVersion.label" default="Test Version" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testVersion" required="" value="${dicrRunInstance?.testVersion}"/>

</div>

