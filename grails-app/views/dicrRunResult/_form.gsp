<%@ page import="com.mphrx.apiAutomation.DicrRunResult" %>



<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'collectionsCountInBaseEnvironment', 'error')} required">
	<label for="collectionsCountInBaseEnvironment">
		<g:message code="dicrRunResult.collectionsCountInBaseEnvironment.label" default="Collections Count In Base Environment" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="collectionsCountInBaseEnvironment" type="number" value="${dicrRunResultInstance.collectionsCountInBaseEnvironment}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'collectionsCountInTestEnvironment', 'error')} required">
	<label for="collectionsCountInTestEnvironment">
		<g:message code="dicrRunResult.collectionsCountInTestEnvironment.label" default="Collections Count In Test Environment" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="collectionsCountInTestEnvironment" type="number" value="${dicrRunResultInstance.collectionsCountInTestEnvironment}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'collectionsInBaseButNotInTest', 'error')} ">
	<label for="collectionsInBaseButNotInTest">
		<g:message code="dicrRunResult.collectionsInBaseButNotInTest.label" default="Collections In Base But Not In Test" />
		
	</label>
	

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'collectionsInTestButNotInBase', 'error')} ">
	<label for="collectionsInTestButNotInBase">
		<g:message code="dicrRunResult.collectionsInTestButNotInBase.label" default="Collections In Test But Not In Base" />
		
	</label>
	

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'commonCollectionsCount', 'error')} required">
	<label for="commonCollectionsCount">
		<g:message code="dicrRunResult.commonCollectionsCount.label" default="Common Collections Count" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="commonCollectionsCount" type="number" value="${dicrRunResultInstance.commonCollectionsCount}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'dicrRun', 'error')} required">
	<label for="dicrRun">
		<g:message code="dicrRunResult.dicrRun.label" default="Dicr Run" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="dicrRun" name="dicrRun.id" from="${com.mphrx.apiAutomation.DicrRun.list()}" optionKey="id" required="" value="${dicrRunResultInstance?.dicrRun?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'dicrRunErrors', 'error')} ">
	<label for="dicrRunErrors">
		<g:message code="dicrRunResult.dicrRunErrors.label" default="Dicr Run Errors" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${dicrRunResultInstance?.dicrRunErrors?}" var="d">
    <li><g:link controller="dicrRunError" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="dicrRunError" action="create" params="['dicrRunResult.id': dicrRunResultInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'dicrRunError.label', default: 'DicrRunError')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'error', 'error')} required">
	<label for="error">
		<g:message code="dicrRunResult.error.label" default="Error" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="error" required="" value="${dicrRunResultInstance?.error}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'result', 'error')} required">
	<label for="result">
		<g:message code="dicrRunResult.result.label" default="Result" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="result" required="" value="${dicrRunResultInstance?.result}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dicrRunResultInstance, field: 'testCaseName', 'error')} required">
	<label for="testCaseName">
		<g:message code="dicrRunResult.testCaseName.label" default="Test Case Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="testCaseName" required="" value="${dicrRunResultInstance?.testCaseName}"/>

</div>

