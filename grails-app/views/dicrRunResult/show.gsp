
<%@ page import="com.mphrx.apiAutomation.DicrRunResult" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dicrRunResult.label', default: 'Dicr Run Errors')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		<link rel="stylesheet" href="${resource(dir: 'www/lib/codemirror/lib',file: 'codemirror.css')}" charset="utf-8">
		<link rel="stylesheet" href="${resource(dir: 'www/lib/codemirror/theme',file: 'tomorrow-night.css')}" charset="utf-8">
		<link rel="stylesheet" href="${resource(dir: 'css',file: 'main.css')}" charset="utf-8">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="/resources/demos/style.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<style>
		.CodeMirror {
			border: 1px solid #eee;
			min-height:500px;
		}
		</style>
	</head>
	<body>
	<a href="#list-dicrRunError" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
	<div class="nav" role="navigation">
		<ul>
			<li><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			<li><g:link controller="dicrRun" action="index">Dicr Runs</g:link></li>
			<li><g:link controller="dicrRun" action="show" id="${dicrRunResultInstance.dicrRun.id}">Dicr Run Result</g:link>
			</li>
		</ul>
	</div>
	<div id="list-dicrRunError" class="content scaffold-list" role="main">
		<h1><g:message code="default.list.label" args="[entityName]" /></h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table>
			<thead>
			<tr>

				<g:sortableColumn property="id" title="${message(code: 'dicrRunError.id.label', default: 'ID')}" />

				<g:sortableColumn property="collectionName" title="${message(code: 'dicrRunError.collectionName.label', default: 'Collection Name')}" />

				<g:sortableColumn property="countMismatchError" title="${message(code: 'dicrRunError.countMismatchError.label', default: 'Count Mismatch Error')}" />

				<g:sortableColumn property="dateCreated" title="${message(code: 'dicrRunError.dateCreated.label', default: 'Date Created')}" />

				<g:sortableColumn property="ComparisonError" title="${message(code: 'dicrRunError.ComparisonError.label', default: 'Comparison Error')}" />

				%{--
                                        <th><g:message code="dicrRunError.dicrRunResults.label" default="Dicr Run Results" /></th>
                --}%

				%{--
                                        <g:sortableColumn property="lastUpdated" title="${message(code: 'dicrRunError.lastUpdated.label', default: 'Last Updated')}" />
                --}%

			</tr>
			</thead>
			<tbody>
				<g:each in="${dicrRunResultInstance.dicrRunErrors.sort({it.id})}" status="i" var="dicrRunErrorInstance">
				<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

					<td>${dicrRunErrorInstance.id}</td>


					<td><g:link action="show" id="${dicrRunErrorInstance.id}">${fieldValue(bean: dicrRunErrorInstance, field: "collectionName")}</g:link></td>

					<td>${fieldValue(bean: dicrRunErrorInstance, field: "countMismatchError")}</td>

					<td><g:formatDate date="${dicrRunErrorInstance.dateCreated}" /></td>

					<td>
						<g:each in="${dicrRunErrorInstance.comparisonError}" var="comparisonErrorInstance" status="index">
							<button onclick="showErrorMessage('${URLEncoder.encode(comparisonErrorInstance.errorText,'UTF-8').replace("+", "%20")}')" value="Show Error" name="btn_${index+1}" >Show Error ${index+1} </button>
							<button onclick="showJSONDifference(${comparisonErrorInstance.baseJSON},${comparisonErrorInstance.testJSON},this)" value="Show Diff" name="btn_${index+1}" >Show Diff ${index+1}</button>
						</g:each>
					</td>


					%{--
                                            <td>${fieldValue(bean: dicrRunErrorInstance, field: "dicrRunResults")}</td>
                    --}%

					%{--
                                            <td><g:formatDate date="${dicrRunErrorInstance.lastUpdated}" /></td>
                    --}%

				</tr>
			</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate total="${dicrRunErrorInstanceCount ?: 0}" />
		</div>
	</div>

	<header id="header">

		<div class="right">
			<span id="error-message" style="display:none;">Invalid JSON</span>
		</div>
	</header>


	</div>

	<!-- The Modal -->
	<div id="myModal">


	</div>

	<div id = "jsonDiff-modal" style="width: 0px;height: 0px;">

		<div class="json-diff-input" style="width:50%; height: 100%; float:left;">
			<textarea id="json-diff-left"></textarea>
		</div>
		<div class="json-diff-input" style="width:50%; height: 100%;float:left;">
			<textarea id="json-diff-right"></textarea>
		</div>
		<div style="clear:both;"></div>
	</div>

	<script src="${resource(dir: 'www/lib/fast-json-patch/dist/',file: 'json-patch-duplex.min.js')}" charset="utf-8"></script>
	<script src="${resource(dir: 'www/lib/backbone-events-standalone/',file: 'backbone-events-standalone.min.js')}" charset="utf-8"></script>

	<script src="${resource(dir: 'www/lib/codemirror/lib/',file: 'codemirror.js')}" charset="utf-8"></script>
	<script src="${resource(dir: 'www/lib/codemirror/lib/util/',file: 'formatting.js')}" charset="utf-8"></script>
	<script src="${resource(dir: 'www/lib/codemirror/mode/javascript/',file: 'javascript.js')}" charset="utf-8"></script>
	<script src="${resource(dir: 'www/lib/codemirror/addon/edit/',file: 'matchbrackets.js')}" charset="utf-8"></script>
	<script src="${resource(dir: 'js/',file: 'main.js')}" charset="utf-8"></script>

	<script type="text/javascript">
		function showJSONDifference(json1,json2,element){
			var cmLeft = $('.CodeMirror')[0].CodeMirror;
			var cmRight = $('.CodeMirror')[1].CodeMirror;

			cmLeft.setValue("");
			cmRight.setValue("");
			cmLeft.setValue(JSON.stringify(json1,null,4));
			cmRight.setValue(JSON.stringify(json2,null,4));

			$('#jsonDiff-modal').removeAttr('style');
			$('#jsonDiff-modal').dialog( {
				width: "90%",
				maxWidth: "768px",
				height:'580'
			});
			cmLeft.focus();
			cmLeft.setCursor({line: 0, ch: 1});
			cmLeft;
		}

		function showErrorMessage(error) {
			$( "#myModal" ).html("<pre></pre>")
			$( "#myModal pre" ).text(decodeURIComponent(error));
			$( "#myModal" ).dialog({width: "90%",
				maxWidth: "768px",
				height:'580'});
		}

	</script>
	</body>
</html>
