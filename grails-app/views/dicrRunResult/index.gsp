
<%@ page import="com.mphrx.apiAutomation.DicrRunResult" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dicrRunResult.label', default: 'DicrRunResult')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-dicrRunResult" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-dicrRunResult" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="collectionsCountInBaseEnvironment" title="${message(code: 'dicrRunResult.collectionsCountInBaseEnvironment.label', default: 'Collections Count In Base Environment')}" />
					
						<g:sortableColumn property="collectionsCountInTestEnvironment" title="${message(code: 'dicrRunResult.collectionsCountInTestEnvironment.label', default: 'Collections Count In Test Environment')}" />
					
						<g:sortableColumn property="commonCollectionsCount" title="${message(code: 'dicrRunResult.commonCollectionsCount.label', default: 'Common Collections Count')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'dicrRunResult.dateCreated.label', default: 'Date Created')}" />
					
						<th><g:message code="dicrRunResult.dicrRun.label" default="Dicr Run" /></th>
					
						<g:sortableColumn property="error" title="${message(code: 'dicrRunResult.error.label', default: 'Error')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${dicrRunResultInstanceList}" status="i" var="dicrRunResultInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${dicrRunResultInstance.id}">${fieldValue(bean: dicrRunResultInstance, field: "collectionsCountInBaseEnvironment")}</g:link></td>
					
						<td>${fieldValue(bean: dicrRunResultInstance, field: "collectionsCountInTestEnvironment")}</td>
					
						<td>${fieldValue(bean: dicrRunResultInstance, field: "commonCollectionsCount")}</td>
					
						<td><g:formatDate date="${dicrRunResultInstance.dateCreated}" /></td>
					
						<td>${fieldValue(bean: dicrRunResultInstance, field: "dicrRun")}</td>
					
						<td>${fieldValue(bean: dicrRunResultInstance, field: "error")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${dicrRunResultInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
