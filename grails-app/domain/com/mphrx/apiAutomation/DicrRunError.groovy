package com.mphrx.apiAutomation

import com.mongodb.BasicDBObject

class DicrRunError {

    static constraints = {
    }

    static mapping = {
        collection "dicrRunError"
    }

    static mapWith = "mongo"

    static belongsTo = [dicrRunResult:DicrRunResult]
    static embedded = ['comparisonError']

    String collectionName
    String countMismatchError
    List<ComparisonError> comparisonError
    Date dateCreated
}
