package com.mphrx.apiAutomation

class DicrRun {

    static constraints = {
    }

    static mapping = {
        collection "dicrRun"
    }

    static mapWith = "mongo"

    static hasMany = [dicrRunResults:DicrRunResult]

    Date startTime
    Date endTime
    String executor
    String baseServer
    String baseVersion
    String testServer
    String testVersion
    Date dateCreated
    Date lastUpdated
}
