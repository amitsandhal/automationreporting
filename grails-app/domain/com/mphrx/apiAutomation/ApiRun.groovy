package com.mphrx.apiAutomation

import java.util.concurrent.Executor

class ApiRun {

    static constraints = {

    }

    static mapping = {
        collection "apiRun"
    }

    static mapWith = "mongo"

    static hasMany = [apiRunResults:ApiRunResult]

    Date startTime
    Date endTime
    String executor
    String baseEnvironmentUrl
    String baseEnvironmentVersion
    String testEnvironmentUrl
    String testEnvironmentVersion
    Date dateCreated
    Date lastUpdated
    String runmode


}
