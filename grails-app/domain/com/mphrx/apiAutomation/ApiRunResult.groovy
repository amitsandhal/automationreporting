package com.mphrx.apiAutomation

class ApiRunResult {

    static constraints = {

    }


    static mapping = {
        collection "apiRunResult"
        baseResponse type: 'text'
        testResponse type: 'text'
        error type: 'text'
    }

    static mapWith = "mongo"

    static belongsTo = [apiRun:ApiRun]

    String testCaseName
    String moduleName
    String api
    String payload
    String baseResponse
    String testResponse
    String baseResponseCode
    String testResonseCode
    String result
    String error
    Date lastUpdated
    Date dateCreated

}
