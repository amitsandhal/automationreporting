package com.mphrx.apiAutomation

class DicrRunResult {

    static constraints = {
    }

    static mapping = {
        collection "dicrRunResult"
    }

    static mapWith = "mongo"

    static belongsTo = [dicrRun:DicrRun]
    static hasMany = [dicrRunErrors:DicrRunError]

    String testCaseName
    String result
    int collectionsCountInBaseEnvironment
    int collectionsCountInTestEnvironment
    int commonCollectionsCount
    List<String> collectionsInBaseButNotInTest
    List<String> collectionsInTestButNotInBase
    String error
    Date lastUpdated
    Date dateCreated

}
