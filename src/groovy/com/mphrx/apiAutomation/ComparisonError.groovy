package com.mphrx.apiAutomation

/**
 * Created by amitsandhal on 11/29/2016.
 */
class ComparisonError {

    String testJSON
    String errorText
    String baseJSON


    String getBaseJSON() {
        return baseJSON
    }

    void setBaseJSON(String baseJSON) {
        this.baseJSON = baseJSON
    }

    String getTestJSON() {
        return testJSON
    }

    void setTestJSON(String testJSON) {
        this.testJSON = testJSON
    }

    String getErrorText() {
        return errorText
    }

    void setErrorText(String errorText) {
        this.errorText = errorText
    }

    public String toString(){
        return "{testJSON: ${testJSON}, errorText : ${errorText}, baseJSON : ${baseJSON}"
    }
}
